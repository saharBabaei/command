from cmd import Cmd
import sys

class MyPrompt(Cmd):
    prompt = 'command> '
    intro = "Enter your command :"
    id = "10"

    def do_exit(self):
        print("Bye")
        return True

    def do_createCharacter(self,text):
        text_array = text.split()
        if text_array:
            print("Character "+ "2" +" has been created") # id check shavad
            print("Info1 : " + text_array[0])
            print("Info2 : " + text_array[1])
            print("Info3 : " + text_array[2])
        else:
            print("Error -> Character information is not complete!")

    def default(self, inp):
        if inp == 'bye' or inp == 'exit':
            return self.do_exit()
        else:
            print("Error -> Invalid command!")

    do_EOF = do_exit

if __name__ == '__main__':
    MyPrompt().cmdloop()